from django.http import JsonResponse
from .models import Attendee
from .models import ConferenceVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
        "created",
        "conference",
    ]
    # encoders = {
    #     "conference": ConferenceVODetailEncoder(),
    # }


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


@require_http_methods(["POST", "GET"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendee = Attendee.objects.filter(conference_id=conference_vo_id)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )
    else:
        data = json.loads(request.body)
        try:
            conference_href = f"/api/conferences/{conference_vo_id}"
            conference = ConferenceVO.objects.get(import_href=conference_href)
            data["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**data)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        data = json.loads(request.body)
        try:
            if "conference" in data:
                conference = Conference.objects.get(
                    conference_id=data["conference"]
                )
                data["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid request"},
                status=400,
            )
        Attendee.objects.filter(id=id).update(**data)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee, encoder=AttendeeDetailEncoder, safe=False
        )
